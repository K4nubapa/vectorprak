﻿#include <iostream>

class Vector
{
public:
    Vector() : x(7), y(3), z(5)
    {}

    Vector(double _x, double _y, double _z) : x(_x), y(_y), z(_z)
    {}

    void Show()
    {
        std::cout << sqrt(pow(x, 2) + pow(y, 2) + pow(z, 2)) << "\n";
    }

private:
    double x;
    double y;
    double z;
};

int main()
{
    Vector v (3, 7, 2);
    Vector v1(5, 1, 4);
    Vector v2;

    v.Show();
    v1.Show();
    v2.Show();
}

